source("code/Common.R")

class0FilePath = commandArgs()[7]
class1FilePath = commandArgs()[8]
description = commandArgs()[9]
outFilePattern = commandArgs()[10]

class0Data = read.table(class0FilePath, sep="\t", stringsAsFactors=F, header=T, row.names=NULL, check=F)
class1Data = read.table(class1FilePath, sep="\t", stringsAsFactors=F, header=T, row.names=NULL, check=F)

data = rbind(class0Data, class1Data)
data$NAME = sub("RANDOM", "Random", data$NAME)
data$NAME = sub("SIGNAL", "Signal", data$NAME)

numSignal = as.integer(sapply(data[,1], function(x) { sub("Signal", "", strsplit(x, "_")[[1]][2]) }))

plotSimulatedResults(description, numSignal, data$"FDR q-val", sub("metric", "FDR", outFilePattern), "FDR q-value")
plotSimulatedResults(description, numSignal, data[,"NOM p-val"], sub("metric", "p-value", outFilePattern), "p-value")
