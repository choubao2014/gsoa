import os, sys, glob

inFilePath1 = sys.argv[1]
inFilePath2 = sys.argv[2]

inFile1 = open(inFilePath1)
inFile2 = open(inFilePath2)

inFile1.readline()
inFile2.readline()

areIdentical = True

for line1 in inFile1:
    line2 = inFile2.readline()
    if line1 != line2:
        areIdentical = False
        break

inFile2.close()
inFile1.close()

if not areIdentical:
    print inFilePath1
    for line in file(inFilePath1):
        print line.rstrip()
    print inFilePath2
    for line in file(inFilePath2):
        print line.rstrip()

print areIdentical
