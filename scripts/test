#!/bin/bash

set -o errexit

./scripts/generate_test_data

function runTest {
  # We currently only support two classes, but it could be extended easily to multiple classes
  numClasses=$1
  permuteSeed=$2

  outMetricsFilePath=Results/Test/Simulated_Classes${numClasses}.txt
  outPredictionsFilePath=Results/Test/Simulated_Classes${numClasses}_Predictions.txt

  if [ "$permuteSeed" != "" ]
  then
    outMetricsFilePath=Results/Test/Simulated_Classes${numClasses}_Permuted${permuteSeed}.txt
    outPredictionsFilePath=Results/Test/Simulated_Classes${numClasses}_Predictions_Permuted${permuteSeed}.txt
  fi

  rm -f $outMetricsFilePath
  ./scripts/run Data/Simulated/Data.txt Data/Simulated/Classes${numClasses}.txt Data/Simulated/genesets.gmt $outMetricsFilePath "" $outPredictionsFilePath 100 "" "" "svmrbf" "$permuteSeed"

  function checkOutput {
    outFilePath=$1

    expectedOutFilePath=Expected_Test_Results/$(basename $outFilePath)
    diffResult=$(python code/AreFilesIdentical.py $outFilePath $expectedOutFilePath)

    if [ "$diffResult" != "True" ]
    then
      echo Test $outFilePath [FAILED]
      echo $outFilePath and $expectedOutFilePath are not identical.
      exit 1
    fi

    echo Test $outFilePath [PASSED]
  }

  checkOutput $outMetricsFilePath
  checkOutput $outPredictionsFilePath
}

runTest 2
runTest 2 1
